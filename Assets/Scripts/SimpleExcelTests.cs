﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Debug = UnityEngine.Debug;

public class SimpleExcelTests : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ExportData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    public const string RootPath = "./Logs/";
    
    private string GetRootPath()
    {
        return Path.GetFullPath(RootPath).Replace("\\", "/");
    }
    
    private void ExportData()
    {
        var root = GetRootPath();
        var fileName = $"export-{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.xls";
        var path = Path.Combine(root, fileName);
        
        using (var fs = GetFileStream(path, FileMode.Create))
            WriteWorkbook(root, fs);

        OpenFile(Path.Combine(root, fileName));
    }

    private void WriteWorkbook(string root, FileStream fs)
    {
        var wb = new HSSFWorkbook();
    
        // Assuming all directories are people
        var folders = Directory.GetDirectories(root);
        foreach (var folderPath in folders)
        {
            var files = Directory.GetFiles(folderPath);
    
            if (!files.Any())
                continue;
    
            var folder = Path.GetFileName(folderPath);
            var sheet = wb.CreateSheet(folder);
            sheet.CreateRow(0);
    
            var previousExt = string.Empty;
    
            foreach (var file in files)
            {
                var ext = Path.GetExtension(file);
                bool addHeaders = false;
    
                if (previousExt != ext)
                {
                    addHeaders = true;
                    // add an empty row when not at the start of the sheet
                    if (sheet.LastRowNum > 0)
                        sheet.CreateRow(sheet.LastRowNum);
                }
    
                if (ext == CsvReadWriter.Ext)
                {
                    if (addHeaders)
                    {
                        var row = sheet.CreateRow(sheet.LastRowNum);
                        for (int i = 0; i < CsvHelper.ColumnCount; ++i)
                        {
                            var cell = row.CreateCell(i);
                            cell.SetCellValue(CsvHelper.Columns[i]);
                        }
                    }
    
                    ParseCsv(file, sheet);
                }
                else
                {
                    Debug.LogWarning($"Unsupported Ext: '{ext}'");
                    continue;
                }
    
                previousExt = ext;
            }
        }
    
        wb.Write(fs);
        wb.Close();
    }

    private void OpenFile(string path)
    {
        Process.Start(path);
    }
    
    private void ParseCsv(string filePath, ISheet sheet)
    {
        try
        {
            var data = CsvReadWriter.LoadFromFile(filePath);
    
            if (!data[0].SequenceEqual(CsvHelper.Columns))
                return;
    
            for (int i = 1; i < data.Count; ++i)
            {
                var lineData = data[i];
                var row = sheet.CreateRow(sheet.LastRowNum + 1);
                for (int dataI = 0; dataI < lineData.Length; ++dataI)
                {
                    var cell = row.CreateCell(dataI);
                    cell.SetCellValue(lineData[dataI]);
                }
            }
            
            // Empty line between files
            sheet.CreateRow(sheet.LastRowNum + 1);
        }
        catch (Exception e)
        {
            // probably not a correct file.
            Debug.LogError(e.ToString());
        }
    }

    private FileStream GetFileStream(string path, FileMode mode = FileMode.OpenOrCreate, FileAccess access = FileAccess.ReadWrite, FileShare share = FileShare.ReadWrite)
    {
        return new FileStream(path, mode, access, share);
    }
    
    
    public class CsvReadWriter
{
    public const string Ext = ".csv";
    private static List<string> _dataBuffer;

    /// ================================================================================================================
    /// READER
    public delegate void ReadLineDelegate(int i, string[] data);

    public static void LoadFromFile(string filePath, ReadLineDelegate readAction)
    {
        LoadFromString(File.ReadAllText(filePath), readAction);
    }
    
    public static List<string[]> LoadFromFile(string filePath)
    {
        var dt = new DataTable();
        var dataList = new List<string[]>();
        LoadFromString(File.ReadAllText(filePath), (i, data) =>
        {
            dataList.Add(data);
        });

        return dataList;
    }

    public static void LoadFromString(string content, ReadLineDelegate readAction)
    {
        // read char by char and when a , or \n, perform appropriate action
        if (_dataBuffer == null) _dataBuffer = new List<string>();
        else _dataBuffer.Clear();
        
        int currI = 0; // index in the file
        int currN = 0; // line in the file
        
        do
        {
            ReadLine(content, ref currI);
            readAction(currN++, _dataBuffer.ToArray());
            _dataBuffer.Clear();

        } while (currI < content.Length);
    }

    public static string[] TakeLine(ref string content)
    {
        int moved = 0;
        ReadLine(content, ref moved);
        var data = _dataBuffer.ToArray();
        _dataBuffer.Clear();

        content = content.Substring(moved);
        
        return data;
    }

    private static void ReadLine(string content, ref int currI)
    {
        StringBuilder currItem = new StringBuilder("");
        bool hasQuotes = false; // managing quotes
        var i = 0;
        while (currI < content.Length)
        {
            char c = content[currI++];

            switch (c)
            {
                case '"':
                    if (!hasQuotes)
                    {
                        hasQuotes = true;
                    }
                    else
                    {
                        if (currI == content.Length)
                        {
                            // end of file
                            hasQuotes = false;
                            goto case '\n';
                        }
                        else if (content[currI] == '"')
                        {
                            // double quote, save one
                            currItem.Append("\"");
                            currI++;
                        }
                        else
                        {
                            // leaving quotes section
                            hasQuotes = false;
                        }
                    }

                    break;
                case '\r':
                    // ignore it completely
                    break;
                case ',':
                    goto case '\n';
                case '\n':
                    if (hasQuotes)
                    {
                        // inside quotes, this characters must be included
                        currItem.Append(c);
                    }
                    else
                    {
                        // end of current item
                        _dataBuffer.Add(currItem.ToString());
                        currItem.Length = 0;
                        if (c == '\n' || currI == content.Length)
                        {
                            // also end of line
                            return;
                        }
                    }

                    break;
                default:
                    // other cases, add char
                    currItem.Append(c);
                    break;
            }
        }
    }

    /// ================================================================================================================
    /// WRITER
    public static string GetSeparator()
    {
        var pointSeparator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;

        if (pointSeparator.Equals(","))
            return ";";

        if (pointSeparator.Equals("."))
            return ",";

        return ";";
    }

    public static string GetAsLine(IEnumerable<string> data)
    {
        var sep = GetSeparator();
        
        var formattedData = data
            .Select(x => (x ?? string.Empty)
                .Replace(@"\", @"\\") // backslash slashes
                .Replace("\"", "\\\"") // backslash quotes
            )
            .Select(x => $"\"{x}\"") // add quotes around the item
            .ToArray();

        return string.Join(sep, formattedData);
    }
}
    public static class CsvHelper
    {
        public static readonly string[] Columns = new[] {
            "Date", "Time", "Application", "User", "Elapsed Time", "Task", "Step", "Type", "Action", "State"
        };
		
        public static readonly int ColumnCount = Columns.Length;
    }
}
